#Snap Say 0.1.0

Snap Say is a package making it super easy to create PHP CLI programs in the style of cowsay.

Using the Snap\Say package you too can easily make cowsay style CLI programs using PHP!

##Installation
This library requires PHP 5.4 or later; we recommend using the latest available version of PHP as a matter of principle.
It is installable and autoloadable via Composer as snap/say.
```bash
composer require snap/say
```

##Use 

Download the BobSay package from https://bitbucket.com/alexfraundorf_com/snap-bobsay as an example and/or template.

1. Create a composer.json file requiring the snap\say package.
See composer.json in the BobSay package

2. Create a class (ie: Bob) extending \Snap\SayAbstract and override the $title, $description and $version properties.
Optionally override the protected say method to customize the format of the output and add some ASCII art to be really flashy!
See Bob.php

3. Create a text file with a quote on each line.
See quotes.txt

4. Create your command script, the script that you will call from the command line, for example: bobsay.php
Create an instance of your class, optionally provide a path to your quotes file and call the run method, making sure to pass it the $argv variable.  $argv is an array holding any arguments passed to the bobsay command.
See bobsay.php

5. Bonus for Linux/Mac users
Make bobsay.php executable, and move it one level up, renaming it bobsay and have the whole project in a directory that is in your include path (like ~/bin) so you can call the program from anywhere.
```bash
cd vendor/snap/bobsay/src
chmod +x bobsay.php
mv bobsay.php ../../../../bobsay
```

## Using Your Created Say Program
On Linux/Mac, assuming you followed the instructions above and the say file is in a directory in your include path, you can call the program from anywhere.
```bash
# random quote
bobsay
# tell Bob what to say
bobsay "My name is Bob and I think you are awesome!"
# pipe output from another program into Archer's mouth (you must install fortune for this example)
fortune | bobsay
```
On Windows, you will need to either be in the same directory as the bobsay.php file or provide an absolute path to it.
```bash
# random quote
php bobsay.php
# tell Bob what to say
php bobsay.php "My name is Bob and I think you are awesome!"
```

##Packages built using Snap/Say

Example BobSay at https://bitbucket.com/alexfraundorf_com/snap-bob-say or on Packagist at Snap/BobSay

ArcherSay at https://bitbucket.com/alexfraundorf_com/snap-archer-say or on Packagist at Snap/ArcherSay

Tell me about your package that you wrote using Snap/Say to have it included here!

##Troubleshooting
1. Permission denied - make sure you made the bobsay/bobsay.php file executable
2. bobsay: command not found - the bobsay file is not in your include path.  Add its parent directory to your include path, call it from within its parent directory or use an absolute path to it.
3. PHP not found - make sure PHP is installed properly by running php -v  If it isn't install it using your package manager.

Other questions or problems: Feel free to contact me through AlexFraundorf.com and I'll do my best to help :-)

Happy PHPing!
