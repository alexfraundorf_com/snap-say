<?php
/*
 * Interface for Say command line program.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2019, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\Say
 * @version 0.1.0 02/21/2019
 * @since 0.1.0 02/21/2019
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\Say;

interface SayInterface {

    
    /**
     * Setter for the path to the quotes text file.
     * 
     * @param string $file_path the path to the file 
     * @return \Snap\SayAbstract
     * @throws \InvalidArgumentException
     */
    public function setPathToQuotes($file_path);
    
    
    /**
     * Run the CLI application.
     * 
     * @param array $argv
     */
    public function run($argv);

        
}
