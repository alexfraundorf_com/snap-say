<?php
/*
 * Abstract class to simplify creating a cowsay style command line program.
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2019, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\Say
 * @version 0.1.0 02/26/2019
 * @since 0.1.0 02/11/2019
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\Say;

abstract class SayAbstract implements SayInterface {
    
    /** 
     *
     * @var \Lijinma\Commander
     */
    protected $Commander;
    
    /**
     *
     * @var string path to quotes text file
     */
    protected $path_to_quotes;
    
    /**
     *
     * @var int don't add words to the line if it is already this long
     */
    protected $line_character_limit = 30;
    
    /**
     *
     * @var string title of the CLI application
     */
    protected $title = 'Snap Say Abstract Class';
    
    /**
     *
     * @var string description and simple useage instructions for the CLI application
     */
    protected $description = 'Allow for easy creation of cowsay style CLI apps '
            . 'in PHP. Enter a quote after the command to have it said or just '
            . 'enter the command for a randomly selected quote. Piping output '
            . 'from another program is also supported.';
    
    /**
     *
     * @var string version 
     */
    protected $version = '0.0.1 (02/21/2019)';
    
        
    
    /**
     * Constructor
     * 
     * @version 0.1.0 02/12/2019
     * @since 0.1.0 02/11/2019
     */
    public function __construct() {
        $this->Commander = new \Lijinma\Commander($this->title, $this->description);
    }
    
    
    /**
     * Setter for the path to the quotes text file.
     * 
     * @param string $file_path the path to the file 
     * @return \Snap\SayAbstract
     * @throws \InvalidArgumentException
     * @version 0.1.0 02/11/2019
     * @since 0.1.0 02/11/2019
     */
    public function setPathToQuotes($file_path) {
        if(!is_readable($file_path)) {
            throw new \InvalidArgumentException('Path to quotes file: ' 
                    . $file_path . ' does not exist or is not readable.');
        }
        $this->path_to_quotes = $file_path;
        return $this;
    }
    
    
    /**
     * Run the CLI application.
     * 
     * @param array $argv
     * @version 0.1.0 02/26/2019
     * @since 0.1.0 02/11/2019
     */
    public function run($argv) {
        // check for standard in input
        // credit: https://stackoverflow.com/questions/18137942/how-can-i-check-if-stdin-exists-in-php-php-cgi
        $stdin = '';
        $fh = fopen('php://stdin', 'r');
        $read  = array($fh);
        $write = NULL;
        $except = NULL;
        if ( stream_select( $read, $write, $except, 0 ) === 1 ) {
            while ($line = fgets( $fh )) {
                    $stdin .= $line;
            }
        }
        fclose($fh);
        // replace tabs with a few spaces and remove new lines
        $stdin = trim(str_replace(PHP_EOL, ' ', str_replace("\t", '  ', $stdin)));
        // if std in exists, append it to $argv
        if($stdin) {
            $argv[] = $stdin;
        }
        
        $this->Commander
                ->version($this->version)
                ->parse($argv)
                ;
        
        // see if there is a quote or standard input provided
        if(isset($argv[1])) {
            $this->say((string) $argv[1]);
        }
        // otherwise get a random line from the quotes file
        $quote = $this->getRandomQuote();
        if($quote) {
            $this->say((string) $quote);
        }
        // if no quote or file are provided
        else {
            $this->say('I have nothing to say right now.');
        }
        exit(0);
    }
    
    
    /**
     * "Says" the quote by printing it to the CLI.
     * 
     * Override this method in your concrete class to customize the appearance 
     *  of the quote or to add ASCII artwork!
     * 
     * @param string $quote
     * @version 0.1.0 02/12/2019
     * @since 0.1.0 02/11/2019
     */
    protected function say($quote) {
        $max_length = $this->line_character_limit;
        $words = explode(' ', $quote);
        $lines = $this->getLines($words);
        $output_lines = '';
        foreach($lines as $line) {
            $line_out = '|     ' . $line;
            $spaces = 41 - strlen($line);
            for($i=1; $i<=$spaces; $i++) {
                $line_out .= ' ';
            }
            $line_out .= ' |' . PHP_EOL;
            if(strlen($line_out) > $max_length) {
                $max_length = strlen($line_out) - 6;
            }
            $output_lines .= $line_out;
        }
        $output = '  -';
        for($i=1; $i<=$max_length; $i++) {
            $output .= '-';
        }
        $output .= '-' . PHP_EOL . ' /';
        for($i=1; $i<=$max_length; $i++) {
            $output .= ' ';
        }
        $output .= '  \\' . PHP_EOL
                . $output_lines
                . ' \\';
        for($i=1; $i<=$max_length; $i++) {
            $output .= ' ';
        }
        $output .= '  /' . PHP_EOL
                . '  -';
        for($i=1; $i<=$max_length; $i++) {
            $output .= '-';
        }
        $output .= '-' . PHP_EOL;
        fwrite(STDOUT, $output);
        exit(0);
    }
    
    
    /**
     * Returns a random line from the specified quote file or an empty string 
     *  if a quote can't be found.
     * 
     * @return string
     * @version 0.1.0 02/12/2019
     * @since 0.1.0 02/11/2019
     */
    protected function getRandomQuote() {
        if(!$this->path_to_quotes || !is_readable($this->path_to_quotes)) {
            return '';
        }
        $f_contents = file($this->path_to_quotes); 
        return trim((string) $f_contents[rand(0, count($f_contents) - 1)]);
    }
    
    
    /**
     * Breaks the quote up into lines that will fit in the speach bubble.
     * 
     * @param array $words the words in the quote
     * @param array $lines an array of the formatted lines
     * @return array the completed lines array after recursing as needed.
     * @version 0.1.0 02/11/2019
     * @since 0.1.0 02/11/2019
     */
    protected function getLines(array &$words, array &$lines = []) {
        if(!$words) {
            return $lines;
        }
        $line = '';
        while(strlen($line) <= $this->line_character_limit && isset($words[0])) {
            $line .= array_shift($words) . ' ';
        }
        if(strlen(trim($line)) > 0) {
            $lines[] = trim($line);
        }
        if($words) {
            return $this->getLines($words, $lines);
        }
        else {
            return $lines;
        }
    }
    
    
}
